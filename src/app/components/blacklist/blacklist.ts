export interface Blacklist {
    id: string;
    email: string;
}