export class User {
    //id!: string;
    firstName!: string;
    lastName!: string;
    description!: string;
    adress!: string;
    telephone!: string;
    datecreation!: Date;
    email!: string;
    password!: string;
    role!: string;
    //locked!: string;
    //enabled!: string;
}