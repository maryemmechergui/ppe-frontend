import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { User } from './user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user:User = new User();
  constructor(private service: LoginService, private route: Router) { }
    formModel = {
      email: '',
      password: ''
    }  
  ngOnInit(): void {
    if(localStorage.getItem('token') != null)
   // alert("you are not allowed")
    this.route.navigateByUrl('/home');
  }

  onSubmit(form: NgForm) {
    console.log(form.value);
    this.service.login(form.value).subscribe(
      (res: any) => {
        console.log(res)
        console.log(res.access_token)
        localStorage.setItem('token', res.access_token);
        localStorage.setItem('email', res.user);
        alert("Welcome")
        this.route.navigateByUrl('/home');
       
        
      }
      ,error=>{
        alert(console.log() )
        this.route.navigate(['/login']);
  
      
      });
  }

  onLogout() {
    localStorage.removeItem('token');  
    this.route.navigate(['/login']);
  }


}
