import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from './user';

@Injectable({providedIn: 'root'})

export class LoginService {
   private apiServerUrl= environment.apiBaseUrl;
  constructor(private http: HttpClient, private fb: FormBuilder) { }

  formModel = this.fb.group({

   Email: ['', Validators.email],
   Passwords: this.fb.group({
     Password: ['', [Validators.required, Validators.minLength(4)]]
   }),
  
   
 });

 login(formData:any) {
     console.log(formData);
    let  options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };
     const payload = new HttpParams()
  .set('email', formData.email)
  .set('password', formData.password);
     return this.http.post(`${this.apiServerUrl}/login`,payload,options)
  }

  public getUsersByEmail(email: string): Observable<User[]> {
    console.log(email);
    return this.http.get<User[]>(`${this.apiServerUrl}/user/findByEmail/${email}`);
  }
  
  roleMatch(allowedRoles:any): boolean {
   var isMatch = false;
   var payLoad = JSON.parse(window.atob(localStorage.getItem('token')!.split('.')[1]));
   var userRole = payLoad.role;
   console.log(payLoad.role);
   allowedRoles.forEach((element: any) => {
     if (userRole == element) {
       isMatch = true;
       return false;
     }
   });
   return isMatch;
 }
}