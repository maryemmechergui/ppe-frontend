export interface User {
    id: number;
    firstName: string;
    lastName: string;
    description: string;
    adress: string;
    telephone: string;
    datecreation: Date;
    email: string;
    password: string;
    role: string;
    //locked: boolean;
    //enabled: boolean;
}