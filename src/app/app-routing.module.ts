import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlacklistComponent } from './components/blacklist/blacklist.component';
import { ForgetpasswordComponent } from './components/forgetpassword/forgetpassword.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { ResetComponent } from './components/reset/reset.component';
import { UserComponent } from './components/user/user.component';
import { VisiteurComponent } from './components/visiteur/visiteur.component';

const routes: Routes = [
{path:"visiteur", component: VisiteurComponent},
{path:"home", component: HomeComponent},
{path:"user", component: UserComponent},
{path:"login", component: LoginComponent},
{path:"registration", component: RegistrationComponent},
{path:"blacklist", component: BlacklistComponent},
{path:"forgetpass", component: ForgetpasswordComponent},
{path:"reset", component: ResetComponent}




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
